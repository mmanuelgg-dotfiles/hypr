# This is an example Hyprland config file.
#
# Refer to the wiki for more information.
#
# Please note not all available settings / options are set here.
# For a full list, see the wiki
#
# info at https://wiki.hyprland.org/
# See https://wiki.hyprland.org/Configuring/Monitors/

monitor=,preferred,auto,1

# Output
# Configure your Display resolution, offset, scale and Monitors here, use `hyprctl monitors` to get the info.

monitor=DP-3,1920x1080@60,2560x0,1
monitor=DP-1,2560x1440@143.97,0x0,1
# monitor=HDMI-A-1,1920x1080@60,0x0,1
# monitor=HDMI-A-2,1920x1080@60,0x0,1
#monitor=HDMI-A-1,1920x1080@60,0x0,1
#workspace=HDMI-A-1,1
#monitor=HDMI-A-2,1920x1080@60,1920x0,1
#workspace=HDMI-A-2,2

# Example :
#monitor=eDP-1,1920x1080@60,0x0,1
#monitor=eDP-1,transform,0
#monitor=eDP-1,addreserved,10,10,10,10
#workspace=eDP-1,1

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
# Setting variables
$mainMod = SUPER
$localBin = $HOME/.local/bin
$scriptsDir = $HOME/.config/hypr/scripts
$hyprDir = $HOME/.config/hypr
$term = kitty
$terminal = kitty
$menu = $scriptsDir/menu
$fullmenu = $scriptsDir/fullmenu
$rofiConfig = $hyprDir/rofi/config
$rofi = rofi -show run -sidebar-mode -config $rofiConfig
$rofiemoji = rofi -show emoji -config $rofiConfig
$rofiwindow = rofi -show window -config $rofiConfig
$volume = $scriptsDir/volume
$backlight = $scriptsDir/brightness
$screenshot = $scriptsDir/screenshot
$lock = $scriptsDir/lockscreen
$colorpicker = $scriptsDir/colorpicker
$wofi_beats = $scriptsDir/wofi-beats
$files = thunar
$browser = firefox
# $editor = code
$editor = emacsclient -c -a 'emacs'

# Try SwayNotificationCenter (swaync) instead of mako or dunst

# Some default env vars
env = XCURSOR_SIZE,24

# Worked with Nvidia without this before, but added to see if screensharing works
env = LIBVA_DRIVER_NAME, nvidia
env = XDG_SESSION_TYPE, wayland
# env = GBM_BACKEND, nvidia-drm # May encounter creashes with Firefox
env = __GLX_VENDOR_LIBRARY_NAME, nvidia # This supposedly breaks screensharing with Discord, Zoom and the like
# env = WLR_NO_HARDWARE_CURSORS, 1

# Startup
exec-once = $scriptsDir/startup
exec-once = swaybg -m fill -i $HOME/.config/hypr/bg/all/wallpaper.png
exec-once = $scriptsDir/setWallpapers
exec-once = /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1
exec-once = dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once = systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once = $scriptsDir/portal
# exec-once = wl-paste --type text --watch cliphist store # clipboard store text data
# exec-once = wl-paste --type image --watch cliphist store # clipboard store image data

exec-once = wlsunset -t 3600 -T 6500 -s 19:00 -S 07:00 -d 1800
exec-once = hypridle
exec-once = bitwarden-desktop
# exec-once = $scriptsDir/sleep
# with lock - hyprctl
# exec-once = swayidle -w timeout 1200 '$lock' timeout 1200 'hyprctl dispatch dpms off' resume 'hyprctl dispatch dpms on' before-sleep '$lock'

# with lock - swaylock
# exec-once = swayidle -w timeout 1200 'swaylock -f -c 111111' timeout 1200 '' resume '' before-sleep 'swaylock -f -c 111111'

# without lock
# exec-once = swayidle -w  timeout 1200 'hyprctl dispatch dpms off' resume 'hyprctl dispatch dpms on'


# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
  # qwerty
  # kb_layout = us
  kb_layout = es

  # azerty
  #kb_layout = be

  kb_variant =
  kb_model =
  kb_options =
  kb_rules =

  sensitivity = 0.0 # -1.0 - 1.0, 0 means no modification
  accel_profile = flat # flat, adaptative, custom
  follow_mouse = 1
  numlock_by_default = 1

  touchpad {
    natural_scroll = true
    tap-to-click = true
    drag_lock = true
    disable_while_typing = true
  }

  #sensitivity = 0.0 # -1.0 - 1.0, 0 means no modification
}

misc {
  disable_hyprland_logo = true
  disable_splash_rendering = true
  mouse_move_enables_dpms = true
  # no_direct_scanout = true #for fullscreen games

}

binds {
  workspace_back_and_forth = true
}

general {
  # See https://wiki.hyprland.org/Configuring/Variables/ for more

  gaps_in = 3
  gaps_out = 7
  border_size = 3

  resize_on_border = true
  hover_icon_on_border = true

  #one color
  #col.active_border = rgba(7aa2f7aa)

  #two colors - gradient

  # col.active_border = rgba(7aa2f7aa) rgba(c4a7e7aa) 45deg
  col.active_border = rgba(bd93f9aa) rgba(c4a7e7aa) 45deg
  col.inactive_border = rgba(414868aa)

  layout = master
  #layout = dwindle
}

decoration {
  # See https://wiki.hyprland.org/Configuring/Variables/ for more

  rounding = 7
  # blur = true
  # blur_size = 3
  # blur_passes = 3
  # blur_new_optimizations = true

  #active_opacity = 1.0
  #inactive_opacity = 0.9
  #fullscreen_opacity = 1.0

  # drop_shadow = true
  # shadow_range = 4
  # shadow_render_power = 3
  # col.shadow = rgba(1a1a1aee)

  # dim_inactive = true
  # dim_strength = 0.2
}

group {
  # See https://wiki.hyprland.org/Configuring/Variables/ for more
  # col.active = rgba(7aa2f7aa)
  # col.inactive = rgba(414868aa)
  insert_after_current = true
  focus_removed_window = true
  col.border_active = 0xffffb86c
  col.border_inactive = 0xff282737
  col.border_locked_active = 0xff7aa2f7
  col.border_locked_inactive = 0xff414868

  groupbar {
    font_size = 10
    gradients = false
    render_titles = false
    text_color = 0xffbd93f9
    col.active = 0xffffb86c
    col.inactive = 0xff6272a4
    col.locked_active = 0xbd93f9
    col.locked_inactive = 0xff44475a
  }

}

# blurls = waybar

animations {
  enabled = true

  # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

  bezier = myBezier, 0.05, 0.9, 0.1, 1.05

  animation = windows, 1, 7, myBezier
  animation = windowsOut, 1, 7, default, popin 80%
  animation = border, 1, 10, default
  animation = fade, 1, 7, default
  animation = workspaces, 1, 6, default
}

dwindle {
  # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
  pseudotile = true
  preserve_split = true
}

master {
  # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
  # new_is_master = true
  mfact = 0.5
}

gestures {
  # See https://wiki.hyprland.org/Configuring/Variables/ for more
  workspace_swipe = false
  workspace_swipe_fingers = 3
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
#device:epic mouse V1 {
#  sensitivity = -0.5
#}

# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more
# Example windowrule v1
# windowrule = float, yad|nm-connection-editor|pavucontrol
# windowrule = float, feh|Gimp|qt5ct

# Example windowrule v2
windowrulev2 = float, center, class:^(pavucontrol)$

# windowrulev2 = workspace 4, class:^(Apache Directory Studio)$
# windowrulev2 = bordercolor rgb(EE4B55) rgb(880808), fullscreen:1
# windowrulev2 = bordercolor rgb(282737) rgb(1E1D2D), floating:1
# windowrulev2 = opacity 0.8 0.8, pinned:1
# windowrulev2 = workspace 10, class:^(Microsoft-edge)$
windowrulev2 = workspace 4 silent, class:^(steam)$, title:^(Steam)$
windowrulev2 = workspace 4 silent, class:^(Minecraft.*).*$, title:^(Minecraft.*).*$
windowrulev2 = workspace 7, class:^(discord)$
windowrulev2 = workspace 7, class:^(org.telegram.desktop)$
windowrulev2 = workspace 8, tile, class:^(Spotify)$, title:^(Spotify)$
windowrulev2 = workspace 9 silent, class:^(thunderbird)$
# windowrulev2 = float,class:^(firefox)$,title:^(Picture-in-Picture)$

# for waybar yay update click
windowrulev2 = animation popin, class:^(update)$, title:^(update)$
windowrulev2 = float, class:^(update)$, title:^(update)$
windowrulev2 = size 60% 50%, class:^(update)$, title:^(update)$
windowrulev2 = center, class:^(update)$, title:^(update)$

# for ttyclock
windowrulev2 = float, class:^(clock)$, title:^(clock)$
windowrulev2 = size 33% 27%, class:^(clock)$, title:^(clock)$
windowrulev2 = center, class:^(clock)$, title:^(clock)$

# Try hyprpanel instead of waybar
$statusbar = killall waybar; $HOME/.config/hypr/scripts/statusbar && disown
# $ironbar = killall ironbar; $HOME/.config/hypr/scripts/ironbar && disown
# Example binds, see https://wiki.hyprland.org/Configuring/Binds/ for more
bind = $mainMod SHIFT, R, exec, hyprctl reload; $statusbar; $ironbar
bind = $mainMod SHIFT, D, exec, $fullmenu
bind = $mainMod, D, exec, $menu
bind = $mainMod SHIFT, Q, killactive
bind = $mainMod SHIFT, Return, exec, $files
bind = $mainMod SHIFT, Space, togglefloating
bind = $mainMod, E, exec, $editor
bind = $mainMod, F, fullscreen
bind = $mainMod, Q, killactive,
bind = $mainMod, R, exec, $rofi
bind = $mainMod, W, exec, $rofiwindow
bind = $mainMod, Return, exec, $term
bind = $mainMod, T, exec, $terminal
bind = $mainMod ALT, Return, exec, $terminal
bind = $mainMod, V, exec, pavucontrol
bind = $mainMod, B, exec, firefox
bind = $mainMod, S, exec, spotify
bind = $mainMod SHIFT, E, exec, $rofiemoji

bind = CTRL ALT, A, exec, xfce4-appfinder
bind = CTRL ALT, C, exec, catfish
bind = CTRL ALT, B, exec, $files
bind = CTRL ALT, E, exec, archlinux-tweak-tool
bind = CTRL ALT, I, exec, nitrogen
bind = CTRL ALT, K, exec, $scriptsDir/lockscreen
bind = CTRL ALT, L, exec, $scriptsDir/lockscreen
bind = CTRL ALT, NEXT, exec, conky-rotate -n
bind = CTRL ALT, P, exec, pamac-manager
bind = CTRL ALT, PREVIOUS, exec, conky-rotate -p
bind = CTRL ALT, Return, exec, $term
bind = CTRL ALT, S, exec, spotify
bind = CTRL ALT, T, exec, $terminal
bind = CTRL ALT, W, exec, arcolinux-welcome-app

bind = CTRL SHIFT, Escape,exec, $term --class bashtop -T bashtop -e bashtop

# change wallpaper
# bind = ALT, n, exec, $scriptsDir/changeWallpaper
# bind = ALT, p, exec, $scriptsDir/changeWallpaper
# bind = ALT, left, exec, $scriptsDir/changeWallpaper
# bind = ALT, right, exec, $scriptsDir/changeWallpaper
# bind = ALT, up, exec, $scriptsDir/changeWallpaperDesktop
# bind = ALT, down, exec, $scriptsDir/changeWallpaperDesktop

# Variety
# trash wallpaper
bind = $mainMod ALT, t, exec, variety -t
# next wallpaper
bind = $mainMod ALT, n, exec, variety -n
bind = $mainMod ALT, right, exec, variety -n
# previous wallpaper
bind = $mainMod ALT, p, exec, variety -p
bind = $mainMod ALT, left, exec, variety -p
# favorite wallpaper
bind = $mainMod ALT, f, exec, variety -f
# pause wallpaper
bind = $mainMod ALT, up, exec, variety --pause
# resume wallpaper
bind = $mainMod ALT, down, exec, variety --resume

# effect
bind = $mainMod ALT, g, exec, $scriptsDir/glassmorphismToggle

bind = $mainMod SHIFT, M, exec, hyprctl dispatch splitratio -0.1
bind = $mainMod, M, exec, hyprctl dispatch splitratio 0.1

bind = $mainMod SHIFT, Y, exec, $term --class clock -T clock -e tty-clock -c -C 7 -r -s -f "%A, %B, %d"
#bind = $mainMod, D, layoutmsg, removemaster
bind = $mainMod, Escape, exec, hyprctl kill
bind = $mainMod, I, layoutmsg, addmaster
bind = $mainMod, J, layoutmsg, cyclenext
bind = $mainMod, K, layoutmsg, cycleprev

# bind = $mainMod, P, pseudo
bind = $mainMod CTRL, Return, layoutmsg, swapwithmaster
bind = $mainMod, Space, exec, $scriptsDir/changeLayout
bind = $mainMod, X, exec, archlinux-logout
bind = $mainMod, Y, exec, $term --class update -T update -e cava # f to cycle through foreground colors

# Special Keys
bind = , xf86audioraisevolume, exec, $volume --inc
bind = , xf86audiolowervolume, exec, $volume --dec
bind = , xf86audiomute, exec, $volume --toggle
bind = , xf86audioplay, exec, playerctl play-pause
bind = , xf86audionext, exec, playerctl next
bind = , xf86audioprev, exec, playerctl previous
bind = , xf86audiostop, exec, playerctl stop
bind = , xf86monbrightnessup, exec, $brightness --inc
bind = , xf86monbrightnessdown, exec, $brightness --dec

# Backlight control
bind = $mainMod SHIFT, equal, submap, backlight
submap = backlight
bind = , equal, exec, $backlight --inc
bind = , minus, exec, $backlight --dec
bind = , escape, submap, reset
submap = reset

# Volume control
# bind = $mainMod, equal, submap, volume
bind = $mainMod SHIFT, V, submap, volume
submap = volume
bind = , equal, exec, $volume --inc
bind = , minus, exec, $volume --dec
bind = , 0, exec, $volume --toggle
bind = , 9, exec, $volume --toggle-mic
bind = , escape, submap, reset
submap = reset

# Resize
binde = $mainMod SHIFT, H, resizeactive,-50 0
binde = $mainMod SHIFT, L, resizeactive,50 0
binde = $mainMod SHIFT, K, resizeactive,0 -50
binde = $mainMod SHIFT, J, resizeactive,0 50

# Move
bind = $mainMod CTRL, H, movewindow, l
bind = $mainMod CTRL, L, movewindow, r
bind = $mainMod CTRL, K, movewindow, u
bind = $mainMod CTRL, J, movewindow, d

# Special workspace
bind = $mainMod SHIFT, U, movetoworkspace, special
bind = $mainMod, U, togglespecialworkspace,

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1
bind = $mainMod, L, workspace, m+1
bind = $mainMod, H, workspace, m-1


# Switching monitors
bind = $mainMod ALT, period, swapactiveworkspaces, 0 1
bind = $mainMod, comma, focusmonitor, -1
bind = $mainMod, period, focusmonitor, +1
bind = $mainMod ALT, H, movecurrentworkspacetomonitor, -1
bind = $mainMod ALT, L, movecurrentworkspacetomonitor, +1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

bind = $mainMod, tab, workspace, e+1
bind = $mainMod SHIFT, tab, workspace, e-1

# Groups
bind = $mainMod, G, togglegroup
bind = $mainMod ALT, C, lockgroups
bind = $mainMod ALT, J, changegroupactive, f
bind = $mainMod ALT, K, changegroupactive, b
bind = $mainMod CTRL, J, movewindoworgroup, l
bind = $mainMod CTRL, K, movewindoworgroup, r

bind = $mainMod, O, exec, $colorpicker
bind = $mainMod SHIFT, O, exec, $term --class hyprpicker --hold -e hyprpicker
bind = $mainMod CTRL, S, exec, $wofi_beats

# Screenshot keybindings
# bind = , Print, exec, $screenshot --now
# bind = $mainMod SHIFT, S, exec, $screenshot --now
# bind = $mainMod SHIFT, S, exec, $screenshot --area
bind = $mainMod SHIFT, S, exec, grimblast --notify copy area 
bind = $mainMod, Print, exec, $screenshot --in5
bind = SHIFT, Print, exec, $screenshot --in10
bind = CTRL SHIFT, S, exec, $screenshot --win
bind = $mainMod CTRL, Print, exec, flameshot gui
bind = $mainMod SHIFT, F, exec, gscreenshot -sn

# Qwerty
# Switch workspaces with mainMod + [0-9]
bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10
bind = $mainMod, P, workspace, e-1
bind = $mainMod, N, workspace, e+1

# Qwerty
# Move active window and follow to workspace
bind = $mainMod CTRL, 1, movetoworkspace, 1
bind = $mainMod CTRL, 2, movetoworkspace, 2
bind = $mainMod CTRL, 3, movetoworkspace, 3
bind = $mainMod CTRL, 4, movetoworkspace, 4
bind = $mainMod CTRL, 5, movetoworkspace, 5
bind = $mainMod CTRL, 6, movetoworkspace, 6
bind = $mainMod CTRL, 7, movetoworkspace, 7
bind = $mainMod CTRL, 8, movetoworkspace, 8
bind = $mainMod CTRL, 9, movetoworkspace, 9
bind = $mainMod CTRL, 0, movetoworkspace, 10
bind = $mainMod CTRL, P, movetoworkspace, -1
bind = $mainMod CTRL, N, movetoworkspace, +1

# Qwerty
# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspacesilent, 1
bind = $mainMod SHIFT, 2, movetoworkspacesilent, 2
bind = $mainMod SHIFT, 3, movetoworkspacesilent, 3
bind = $mainMod SHIFT, 4, movetoworkspacesilent, 4
bind = $mainMod SHIFT, 5, movetoworkspacesilent, 5
bind = $mainMod SHIFT, 6, movetoworkspacesilent, 6
bind = $mainMod SHIFT, 7, movetoworkspacesilent, 7
bind = $mainMod SHIFT, 8, movetoworkspacesilent, 8
bind = $mainMod SHIFT, 9, movetoworkspacesilent, 9
bind = $mainMod SHIFT, 0, movetoworkspacesilent, 10
bind = $mainMod SHIFT, P, movetoworkspacesilent, -1
bind = $mainMod SHIFT, N, movetoworkspacesilent, +1

